----------------------------
# Beverage Maker API Gateway

An API Gateway for the Beverage Maker application in the Death Star Canteen.

----------------------------
## Pre-requisites to build the application from source:
- Java Jdk 1.8

----------------------------
## Instructions to build and run the application from source:

```
   > mvnw.cmd clean install spring-boot:run
```

## API Routing is configured via Web page:
```
    http://localhost:9090/
```